export const ballNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // List of ball to play
export const MaxBallsSelected = 6; // Limit of balls that a user can select for a bet
export const BetProfit = 1.5; // Bet profit
