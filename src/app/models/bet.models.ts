import { Ball } from './ball.models';

export class Bet {
  amount: number = 0;
  betAmount: number = 0;
  winnerBall?: number;
  balls: Ball[] = [];
}
