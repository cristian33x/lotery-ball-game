import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Ball } from 'src/app/models/ball.models';

@Component({
  selector: 'app-ball-list',
  templateUrl: './ball-list.component.html',
  styleUrls: ['./ball-list.component.scss'],
})
export class BallListComponent {
  @Input() balls: Ball[] = [];
  @Input() emptyBalls = 0;
  @Input() disabled = false;
  @Output() ballClicked = new EventEmitter<Ball>();
}
