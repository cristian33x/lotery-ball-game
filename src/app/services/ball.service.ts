import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Ball } from '../models/ball.models';
import { ballNumbers } from '../utils/resources';
import { Bet } from '../models/bet.models';

@Injectable({
  providedIn: 'root',
})
export class BallService {
  private selectedBalls$ = new Subject<Ball[]>();
  private bet$ = new Subject<Bet>();

  /**
   * Get the list of ball to bet
   * @returns array with balls
   */
  getBalls(): Ball[] {
    return ballNumbers.map((ball) => {
      return {
        id: ball,
        value: ball,
        color: 'color-' + ball,
      };
    });
  }

  /**
   * @returns Observable with array of balls selected by the user
   */
  get getSelectedBalls$(): Observable<Ball[]> {
    return this.selectedBalls$.asObservable();
  }

  /**
   * @returns Observable with the bet made by the user
   */
  get getBet$(): Observable<Bet> {
    return this.bet$.asObservable();
  }

  /**
   * Set the list of selected balls
   * @param balls Ball[]
   */
  setSelectedBalls(balls: Ball[]): void {
    this.selectedBalls$.next(balls);
  }

  /**
   * Set the bet made by the user
   * @param bet Bet
   */
  setBet(bet: Bet): void {
    this.bet$.next(bet);
  }

  /**
   * Calculate the winning ball and get the user's winning balls
   * @param bet Bet
   */
  startGame(bet: Bet) {
    bet.winnerBall = Math.floor(Math.random() * ballNumbers.length) + 1; // Generate a random number between 1 and 10
    bet.balls = bet.balls.filter((ball) => ball.value === bet.winnerBall); // Get the balls that the user bet and won
    this.setBet(bet);
  }
}
