import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Ball } from 'src/app/models/ball.models';
import { Bet } from 'src/app/models/bet.models';
import { BallService } from 'src/app/services/ball.service';
import { MaxBallsSelected } from 'src/app/utils/resources';

@Component({
  selector: 'app-bet-slip',
  templateUrl: './bet-slip.component.html',
  styleUrls: ['./bet-slip.component.scss'],
})
export class BetSlipComponent implements OnInit, OnDestroy {
  betAmount = 0;
  emptyBalls = MaxBallsSelected;
  placeBetAgain = false; // true when the user has made one bet, used to change a button message
  selectedBalls: Ball[] = [];
  getSelectedBallsSubs$!: Subscription;
  betControl = new FormControl(5, [Validators.required, Validators.min(5)]);

  constructor(private ballService: BallService) {}

  ngOnInit(): void {
    // Subscription to get changes on the selected balls by the user
    this.getSelectedBallsSubs$ = this.ballService.getSelectedBalls$.subscribe(
      (ballsSelected: Ball[]) => {
        this.selectedBalls = ballsSelected;
        this.placeBetAgain = false; // this is a new bet
        if (!this.selectedBalls.length) {
          this.betAmount = 0; // Reset the betAmount when there are no balls or the game is restarted
        }
        // calculate list of empty balls to display
        this.emptyBalls = MaxBallsSelected - this.selectedBalls.length;
      }
    );
  }

  /**
   * calculate the amount of the bet
   */
  placeBet(): void {
    this.betAmount = this.betControl.value * this.selectedBalls.length;
  }

  /**
   * Create a bet and start the game
   */
  startGame(): void {
    this.placeBetAgain = true;
    const bet: Bet = {
      amount: this.betControl.value,
      betAmount: this.betAmount,
      balls: this.selectedBalls,
    };
    this.ballService.startGame(bet);
  }

  /**
   * delete a ball from the selectedBalls and update this list
   * @param ball Ball
   */
  deleteBall(ball: Ball) {
    // Delete ball after is clicked
    this.selectedBalls.splice(this.selectedBalls.indexOf(ball), 1);
    this.ballService.setSelectedBalls(this.selectedBalls);
  }

  /**
   * destroy the getSelectedBallsSubs subscription before the component get destroy
   */
  ngOnDestroy(): void {
    this.getSelectedBallsSubs$.unsubscribe();
  }
}
