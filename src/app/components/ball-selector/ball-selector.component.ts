import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Ball } from 'src/app/models/ball.models';
import { Bet } from 'src/app/models/bet.models';
import { MaxBallsSelected } from 'src/app/utils/resources';
import { BallService } from '../../services/ball.service';
import { BetProfit } from '../../utils/resources';

@Component({
  selector: 'app-ball-selector',
  templateUrl: './ball-selector.component.html',
  styleUrls: ['./ball-selector.component.scss'],
})
export class BallSelectorComponent implements OnInit, OnDestroy {
  balls: Ball[] = [];
  selectedBalls: Ball[] = [];
  winnerBall!: Ball;
  bet!: Bet | undefined;
  calculatedAmount = 0;
  getBetSubs!: Subscription;
  readonly maxBallsSelected = MaxBallsSelected;

  constructor(private ballService: BallService) {}

  ngOnInit(): void {
    this.balls = this.ballService.getBalls();
    // Subscription to get changes on the user's bet
    this.getBetSubs = this.ballService.getBet$.subscribe((bet: Bet) => {
      this.bet = bet;
      // find the winner ball
      this.winnerBall = this.balls.find(
        (ball) => ball.value === bet.winnerBall
      ) as Ball;
      // Calculate how much money the user earned or lost
      this.calculatedAmount =
        bet.amount * bet.balls.length * BetProfit - bet.betAmount;
    });
  }

  /**
   * Add the ball the user clicked on to the 'selectedBalls' list
   * @param ball Ball
   */
  addBall(ball: Ball): void {
    this.selectedBalls.push(ball);
    this.ballService.setSelectedBalls(this.selectedBalls);
  }

  /**
   * empty the 'selectedBalls' list
   */
  unSelectBalls(): void {
    this.selectedBalls = [];
    this.ballService.setSelectedBalls(this.selectedBalls);
  }

  /**
   * Reset the game
   */
  resetGame(): void {
    this.bet = undefined;
    this.unSelectBalls();
  }

  /**
   * destroy the getBetSubs subscription before the component get destroy
   */
  ngOnDestroy(): void {
    this.getBetSubs.unsubscribe();
  }
}
